import React, { Component } from 'react';
import { StyleSheet, Image } from 'react-native';
import { Container, Header, Tab, Tabs, TabHeading, Text, Left, Right, Body, Button, ScrollableTab } from 'native-base';
import Constanta from '../helpers/helpers.constanta';
import Icon from 'react-native-vector-icons/Ionicons';

import Home from './screens.home';
import About from './screens.about';
import Contact from './screens.contact';
import Admin from './screens.admin';

export default class ScreenMenu extends Component {
  render() {
    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Image
              source={{ uri: Constanta.logoImage }}
              style={styles.ImageIconStyle}
            />
          </Left>
          <Body />
          <Right>
            <Button transparent>
              <Icon size={32} color={'#fff'} name='log-in-outline' />
            </Button>
          </Right>
        </Header>
        <Tabs renderTabBar={()=> <ScrollableTab />}>
          <Tab heading={<TabHeading>
            <Icon size={24} color={'#fff'} name="home-outline" />
            <Text>Home</Text></TabHeading>}>
            <Home />
          </Tab>
          <Tab heading={<TabHeading>
            <Icon size={24} color={'#fff'} name="information-circle-outline" />
            <Text>About Us</Text></TabHeading>}>
            <About />
          </Tab>
          <Tab heading={<TabHeading>
            <Icon size={24} color={'#fff'} name="chatbubbles-outline" />
            <Text>Contact Us</Text></TabHeading>}>
            <Contact />
          </Tab>
          <Tab heading={<TabHeading>
            <Icon size={24} color={'#fff'} name="cog-outline" />
            <Text>Admin</Text></TabHeading>}>
            <Admin />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  ImageIconStyle: {
    alignItems: "center",
    padding: 10,
    margin: 5,
    height: 32,
    width: 32,
    resizeMode: 'stretch',
  }
});