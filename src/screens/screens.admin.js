import React, { Component } from 'react';
import { Container, Content, List, View, Spinner, ListItem, Text, Card, CardItem, Left, Body } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';

import ComponentMember from '../components/components.member';

export default class ScreenAdmin extends Component {
  render() {
    return (
      <Container>
        <Content style={{ marginBottom: 5 }}>
          <List>
            <ListItem itemDivider>
            <Icon size={24} color={'black'} name="people-outline" />
              <Text> Manage Member</Text>
            </ListItem>
          </List>
          <View>
            <ComponentMember />
          </View>
        </Content>
      </Container>
    );
  }
}