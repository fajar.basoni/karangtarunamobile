import React, { Component } from 'react';
import { Container, Content, List, View, Spinner } from 'native-base';
import { Alert, ActivityIndicator } from 'react-native';

import ComponentDataItem from '../components/components.dataitem';
import ComponentModal from '../components/components.modal';

import { getListInformations } from '../services/services.informations';

export default class ScreenHome extends Component {

  constructor(props) {
    super(props);

    this.state = {
      keywords: '',
      offset: 0,
      limit: 10,
      isLoading: true,
      data: null,
      setModalVisible: false,
      modalArticleData: {}
    }
  }

  handleItemDataOnPress = (articleData) => {
    this.setState({
      setModalVisible: true,
      modalArticleData: articleData
    })
  }

  handleModalClose = () => {
    this.setState({
      setModalVisible: false,
      modalArticleData: {}
    })
  }

  componentDidMount() {
    getListInformations(this.state.keywords,
      this.state.offset,
      this.state.limit)
      .then(data => {
        this.setState({
          isLoading: false,
          data: data
        });
      }, error => {
        Alert.alert('Error', 'Something went wrong!');
      }
      )
  }

  render() {
    let view = this.state.isLoading ? (
      <View>
        <ActivityIndicator animating={this.state.isLoading} />
        <Spinner color='blue' />
      </View>
    ) : (
        <List
          dataArray={this.state.data}
          renderRow={(item) => {
            return <ComponentDataItem onPress={this.handleItemDataOnPress} data={item} />
          }}
        />
      )

    return (
      <Container>
        <Content style={{marginBottom: 5}}>
          {view}
        </Content>
        <ComponentModal showModal={this.state.setModalVisible}
          articleData={this.state.modalArticleData}
          onClose={this.handleModalClose}
        />
      </Container>
    );
  }
}