import React, { Component } from 'react';
import { Container, Content, List, View, Spinner, ListItem, Text, Card, CardItem, Left, Body } from 'native-base';
import { Alert, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import ComponentMemberItem from '../components/components.memberitem';
import Constanta from '../helpers/helpers.constanta';
import { getListMemberships } from '../services/services.memberships';

export default class ScreenAbout extends Component {

  constructor(props) {
    super(props);

    this.state = {
      keywords: '',
      offset: 0,
      limit: 100,
      isLoading: true,
      data: null,
    }
  }

  componentDidMount() {
    getListMemberships(this.state.keywords,
      this.state.offset,
      this.state.limit)
      .then(data => {
        this.setState({
          isLoading: false,
          data: data
        });
      }, error => {
        Alert.alert('Error', 'Something went wrong!');
      }
      )
  }

  render() {
    let view = this.state.isLoading ? (
      <View>
        <ActivityIndicator animating={this.state.isLoading} />
        <Spinner color='blue' />
      </View>
    ) : (
        <List
          dataArray={this.state.data}
          renderRow={(item) => {
            return <ComponentMemberItem
              data={item} />
          }}
        />
      )

    return (
      <Container>
        <Content style={{ marginBottom: 5 }}>
          <List>
            <ListItem itemDivider>
              <Text>{Constanta.titleAbout}</Text>
            </ListItem>
          </List>
          <Card style={{ flex: 0 }}>
            <CardItem>
              <Left>
                <Body>
                  <Text note style={{ textAlign: 'justify' }}>{Constanta.contentAbout}</Text>
                </Body>
              </Left>
            </CardItem>
          </Card>
          <List>
            <ListItem itemDivider>
              <Icon size={32} color={'black'} name='tv-outline' />
              <Text> Visi</Text>
            </ListItem>
          </List>
          <Card style={{ flex: 0 }}>
            <CardItem>
              <Left>
                <Body>
                  <Text note style={{ textAlign: 'center' }}>{Constanta.contentVisi}</Text>
                </Body>
              </Left>
            </CardItem>
          </Card>
          <List>
            <ListItem itemDivider>
              <Icon size={32} color={'black'} name='trophy-outline' />
              <Text> Misi</Text>
            </ListItem>
          </List>
          <Card style={{ flex: 0 }}>
            <CardItem>
              <Left>
                <Body>
                  <Text note style={{ textAlign: 'center' }}>{Constanta.contentMisi}</Text>
                </Body>
              </Left>
            </CardItem>
          </Card>
          <List>
            <ListItem itemDivider>
              <Icon size={32} color={'black'} name='people-outline' />
              <Text> {Constanta.titleMember}</Text>
            </ListItem>
          </List>
          {view}
        </Content>
      </Container>
    );
  }
}