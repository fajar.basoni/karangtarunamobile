import React, { Component } from 'react';
import { Container, Content, List, View, Spinner, ListItem, Text, Card, CardItem, Left, Body } from 'native-base';
import { Alert, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import Constanta from '../helpers/helpers.constanta';
import ComponentMaps from '../components/components.maps';
import ComponentMessage from '../components/components.message';

import { getListMemberships } from '../services/services.memberships';

export default class ScreenContact extends Component {

  constructor(props) {
    super(props);

    this.state = {
      keywords: '',
      offset: 0,
      limit: 100,
      isLoading: true,
      data: null,
    }
  }

  componentDidMount() {
    getListMemberships(this.state.keywords,
      this.state.offset,
      this.state.limit)
      .then(data => {
        this.setState({
          isLoading: false,
          data: data
        });
      }, error => {
        Alert.alert('Error', 'Something went wrong!');
      }
      )
  }

  render() {
    let view = this.state.isLoading ? (
      <View>
        <ActivityIndicator animating={this.state.isLoading} />
        <Spinner color='blue' />
      </View>
    ) : (
        <ComponentMaps />
      )

    return (
      <Container>
        <Content style={{ marginBottom: 5 }}>
          <List>
            <ListItem itemDivider>
              <Icon size={24} color={'black'} name="location-outline" />
              <Text> Our location</Text>
            </ListItem>
          </List>
          <Card style={{ flex: 0 }}>
            <ComponentMaps />
          </Card>
          <Card style={{ flex: 0 }}>
            <CardItem>
              <Icon size={24} color={'black'} name="pin-outline" />
              <Text note >{Constanta.contentAddress}</Text>
            </CardItem>
          </Card>
          <List>
            <ListItem itemDivider>
              <Icon size={24} color={'black'} name="mail-outline" />
              <Text> Drop us a message</Text>
            </ListItem>
          </List>
          <View>
            <ComponentMessage />
          </View>
        </Content>
      </Container>
    );
  }
}