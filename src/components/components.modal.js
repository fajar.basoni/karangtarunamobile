import React, { Component } from 'react';
import { Dimensions, Modal, Share, ScrollView, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Body, Left, Icon, Right, View, Button, Subtitle } from 'native-base';

const webViewHeigh = Dimensions.get('screen').height - 56;
import ComponentDetail from './components.detail';

export default class

  ComponentModal extends Component {

  constructor(props) {
    super(props);
  }

  handleClose = () => {
    return this.props.onClose();
  }

  handleShare = () => {
    const {
      url,
      title
    } = this.props.articleData;
    message = `${title}\n\nReadMore @${url}\n\nShare via RN News App`;
    return Share.share(
      { title, message, url: message },
      { dialogTitle: `Share ${title}` }
    )
  }

  render() {
    const { showModal, articleData } = this.props;
    const { information_title } = articleData;

    if (information_title != undefined) {
      return (
        <Modal
          animationType="slide"
          transparent
          visible={showModal}
          onRequestClose={this.handleClose}
          propagateSwipe
        >
          <Container style={{ margin: 15, marginBottom: 0, backgroundColor: '#fff' }}>
            <Header>
              <Left>
                <Button onPress={this.handleClose} transparent>
                  <Icon name="close-outline" size={24} color={'#fff'} />
                </Button>
              </Left>
              <Body>
                <Subtitle children={information_title} style={{ color: 'white' }} />
              </Body>
              <Right>
                <Button onPress={this.handleShare} transparent>
                  <Icon name="share-outline" size={24} color={'#fff'} />
                </Button>
              </Right>
            </Header>
            <ScrollView>
              <TouchableOpacity>
                <Content contentContainerStyle={{ height: webViewHeigh }}>
                  <ComponentDetail data={this.props.articleData} />
                </Content>
              </TouchableOpacity>
            </ScrollView>
          </Container>
        </Modal>
      )
    }
    else {
      return null;
    }
  }
}