import React, { Component } from 'react';
import { TouchableNativeFeedback } from 'react-native';
import { Text, View } from 'native-base';
import { Card, Divider } from 'react-native-elements';
import ComponentTime from './components.time';

export default class ComponentDataItem extends Component {
  constructor(props) {
    super(props);
    this.data = props.data;
  }

  handlePress = () => {
    const{
      row_id,
      information_id,
      category_id,
      information_title,
      subject,
      contents,
      viewer,
      is_deleted,
      image_path,
      created_date,
      created_by,
      updated_date,
      updated_by,
      category_name,
      category_desc} = this.data;
    
      this.props.onPress({
        row_id,
        information_id,
        category_id,
        information_title,
        subject,
        contents,
        viewer,
        is_deleted,
        image_path,
        created_date,
        created_by,
        updated_date,
        updated_by,
        category_name,
        category_desc});
  }

  render() {
    const { noteStyle, featuredTitleStyle } = styles;
    return (
      <TouchableNativeFeedback
        useForeground
        onPress={this.handlePress}
      >
        <Card
          featuredTitle={this.data.information_title}
          featuredTitleStyle={featuredTitleStyle}
          image={{ uri: this.data.image_path || 'https://makitweb.com/demo/broken_image/images/noimage.png' }}
        >
          <Text style={{ marginBottom: 10 }}>
            {this.data.subject} ...
          </Text>
          <Divider style={{ backgroundColor: '#dfe6e9' }} />
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={noteStyle}>Viewer {this.data.viewer}</Text>
            <ComponentTime date={this.data.created_date} style={noteStyle} />
          </View>
        </Card>
      </TouchableNativeFeedback>
    );
  }
}

const styles = {
  noteStyle: {
    margin: 5,
    fontStyle: 'italic',
    color: '#b2bec3',
    fontSize: 10
  },
  featuredTitleStyle: {
    marginHorizontal: 5,
    textShadowColor: '#00000f',
    textShadowOffset: { width: 3, height: 3 },
    textShadowRadius: 3
  }
};