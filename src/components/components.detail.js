import React, { Component } from 'react';
import { Image, Dimensions } from 'react-native';
import { CardItem, Text, Left, Body, Right, Content, View } from 'native-base';
import moment from 'moment';
import ComponentTime from './components.time';

const { height } = Dimensions.get('window');
const webViewWidth = Dimensions.get('screen').width - 64;

export default class ComponentDetail extends Component {

  state = {
    screenHeight: 0,
  }

  onContentSizeChange = (contentWidth, contentHeight) => {
    this.setState({
      screenHeight: contentHeight
    });
  }

  render() {
    const {
      row_id,
      information_id,
      category_id,
      information_title,
      subject,
      contents,
      viewer,
      is_deleted,
      image_path,
      created_date,
      created_by,
      updated_date,
      updated_by,
      category_name,
      category_desc
    } = this.props.data;

    const time = moment(created_date).calendar();
    const scrollEnabled = this.state.screenHeight > height;

    return (
      <Content>
        <View>
          <CardItem>
            <Left>
              <Body>
                <Text>{created_by}</Text>
                <ComponentTime date={created_date} />
              </Body>
            </Left>
            <Body />
            <Right>
              <Body>
                <Text note style={{ textTransform: "uppercase", textAlign: "right" }}>
                  {category_desc}
                </Text>
              </Body>
            </Right>
          </CardItem>
          <CardItem>
            <Body>
              <Image source={{ uri: image_path }}
                style={{
                  height: 200,
                  width: webViewWidth,
                  flex: 1,
                  alignContent: "center"
                }} />
            </Body>
          </CardItem>
          <CardItem>
            <Left>
              <Body>
                <Text note style={{ textAlign: "justify" }}>
                  {contents}
                </Text>
              </Body>
            </Left>
          </CardItem>
        </View>
      </Content>
    );
  }
}