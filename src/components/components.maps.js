import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Constanta from '../helpers/helpers.constanta';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LONGITUDE_DELTA = Constanta.latitudeDeltaMaps * ASPECT_RATIO;

export default class ComponentMaps extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      region: {
        latitude: Constanta.latitudeMaps,
        longitude: Constanta.longitudeMaps,
        latitudeDelta: Constanta.latitudeDeltaMaps,
        longitudeDelta: LONGITUDE_DELTA,
      }
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={this.state.region}>
          <Marker
            coordinate={{ 
              latitude: this.state.region.latitude, 
              longitude: this.state.region.longitude
            }}
            title={"Sekretariat"}
            description={"Karang Taruna Desa Bojong Nangka"}
          />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 400,
    width: width,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});