import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';

export default class ComponentMemberItem extends Component {

  constructor(props) {
    super(props);
    this.data = props.data;
  }

  render() {
    return (
      <Card style={{ flex: 0 }}>
        <CardItem>
          <Left>
            <Thumbnail source={{ uri: this.data.image_path || 'Image URL' }} />
            <Body>
              <Text>{this.data.name}</Text>
              <Text note>{this.data.department_name}</Text>
            </Body>
          </Left>
        </CardItem>
      </Card>
    );
  }
}