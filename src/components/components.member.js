import React, { Component } from 'react';
import { StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { Text, Form, Item, Input, Picker, Button, Content, DatePicker, View } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import ComponentFileUpload from './components.fileupload';

export default class ComponentMember extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectDegree: '',
      selectDept: '',
      selectStatus: '',
      chosenDate: new Date(),
    };

    this.setDate = this.setDate.bind(this);
  }

  onSelectDegree(value) {
    this.setState({
      selectDegree: value
    });
  }

  onSelectDept(value) {
    this.setState({
      selectDept: value
    });
  }

  onSelectStatus(value) {
    this.setState({
      selectStatus: value
    });
  }

  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Full Name"
            onChangeText={text => this.setState({ fullname: text })} />
        </View>
        <View style={styles.inputView}>
          <Picker
            style={styles.inputText}
            mode="dialog"
            iosIcon={<Icon name="arrow-down" />}
            selectedValue={this.state.selectDegree}
            onValueChange={this.onSelectDegree.bind(this)}
          >
            <Picker.Item label="Select education degree" value="" />
            <Picker.Item label="SD/Sederajat" value="SD/Sederajat" />
            <Picker.Item label="SMP/Sederajat" value="SMP/Sederajat" />
            <Picker.Item label="SMA/Sederajat" value="SMA/Sederajat" />
            <Picker.Item label="Sarjana (S1)" value="Sarjana (S1)" />
            <Picker.Item label="Magister (S2)" value="Magister (S2)" />
            <Picker.Item label="Doctor (S3)" value="Doctor (S3)" />
          </Picker>
        </View>
        <View style={styles.inputView}>
          <Picker
            mode="dialog"
            iosIcon={<Icon name="arrow-down" />}
            selectedValue={this.state.selectDept}
            onValueChange={this.onSelectDept.bind(this)}
          >
            <Picker.Item label="Select depertment" value="" />
            <Picker.Item label="KETUA" value="1" />
            <Picker.Item label="WAKIL KETUA" value="2" />
            <Picker.Item label="SEKRETARIS" value="3" />
            <Picker.Item label="BENDAHARA" value="4" />
          </Picker>
        </View>
        <View style={styles.inputView}>
          <Picker
            mode="dialog"
            iosIcon={<Icon name="arrow-down" />}
            selectedValue={this.state.selectStatus}
            onValueChange={this.onSelectStatus.bind(this)}
          >
            <Picker.Item label="Select status member" value="" />
            <Picker.Item label="Active" value="Active" />
            <Picker.Item label="Inactive" value="Inactive" />
          </Picker>
        </View>
        <View style={styles.inputView}>
          <DatePicker
            placeHolderText="Select date begin period"
            date={this.state.chosenDate}
            onDateChange={this.setDate}
          />
        </View>
        <View style={styles.inputView}>
          <DatePicker
            placeHolderText="Select date end period"
            date={this.state.chosenDate}
            onDateChange={this.setDate}
          />
        </View>
        <Item>
          <ComponentFileUpload />
        </Item>
        <TouchableOpacity style={styles.btn}>
          <Text style={styles.btnText}>SUBMIT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputView: {
    width: "90%",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20,
    borderBottomWidth: 2
  },
  inputText: {
    height: 50,
    color: "black"
  },
  forgot: {
    color: "black",
    fontSize: 11
  },
  btn: {
    width: "90%",
    backgroundColor: "gray",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  btnText: {
    fontWeight: 'bold',
    color: "white",
  }
});
