import React from 'react'
import { Image } from 'react-native'
import PhotoUpload from 'react-native-photo-upload'
import { Label, Item } from 'native-base'
import Constanta from '../helpers/helpers.constanta'

export default class ComponentFileUpload extends React.Component {
  render() {
    return (
      <PhotoUpload
        onPhotoSelect={avatar => {
          if (avatar) {
            console.log('Image base64 string: ', avatar)
          }
        }}
      >
        <Label style={{textAlign:"center"}}>Choose an image</Label>
        <Image
          style={{
            paddingVertical: 20,
            width: 120,
            height: 120,
            borderRadius: 60,
            alignSelf: "center",
          }}
          resizeMode='cover'
          source={{
            uri: Constanta.avatarImage
          }}
        />
      </PhotoUpload>
    )
  }
}