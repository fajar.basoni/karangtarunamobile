import React, { Component } from 'react';
import { StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { Text, View, Textarea } from 'native-base';

export default class ComponentMessage extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Full Name"
            onChangeText={text => this.setState({ fullname: text })} />
        </View>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Email"
            onChangeText={text => this.setState({ email: text })} />
        </View>
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Subject"
            onChangeText={text => this.setState({ subject: text })} />
        </View>
        <View style={styles.areaView}>
          <Textarea
            style={styles.inputText}
            placeholder="Message"
            rowSpan={5} />
        </View>
        <TouchableOpacity style={styles.btn}>
          <Text style={styles.btnText}>SEND</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputView: {
    width: "90%",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20,
    borderBottomWidth: 2
  },
  areaView: {
    width: "90%",
    borderRadius: 25,
    height: 100,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20,
    borderBottomWidth: 2
  },
  inputText: {
    height: 50,
    color: "black"
  },
  forgot: {
    color: "black",
    fontSize: 11
  },
  btn: {
    width: "90%",
    backgroundColor: "gray",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  btnText: {
    fontWeight: 'bold',
    color: "white",
  }
});
