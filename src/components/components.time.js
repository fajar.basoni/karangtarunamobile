import React, { Component } from 'react';
import { Text } from 'native-base';
import moment from 'moment'

export default class ComponentTime extends Component {

  constructor(props) {
    super(props);
    this.data = props.date;
  }

  render() {
    const time = moment(this.data || moment.now()).fromNow();
    return (
      <Text note>{time}</Text>
    )
  }
}