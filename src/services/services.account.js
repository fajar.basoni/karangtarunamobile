import Constanta from '../helpers/helpers.constanta';
const urlService = Constanta.urlApi;

export async function getLogin(username, password){
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "username": username,
        "password": password
      }),
    }

    let datas = await fetch(`${urlService}/Account/GetLogin`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}

export async function insertSignUp(username, 
  password,
  full_name,
  email,
  role){
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "username": username,
        "password": password,
        "full_name": full_name,
        "email": email,
        "role": role
      }),
    }

    let datas = await fetch(`${urlService}/Account/InsertSignUp`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}