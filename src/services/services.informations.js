import Constanta from '../helpers/helpers.constanta';
const urlService = Constanta.urlApi;

export async function getListInformations(keywords,
  offset,
  limit) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "keywords": keywords,
        "offset": offset,
        "limit": limit
      }),
    }

    let datas = await fetch(`${urlService}/Informations/GetListInformations`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}

export async function getDetailInformations(id) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "id": id
      }),
    }

    let datas = await fetch(`${urlService}/Informations/GetDetailInformations`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}

export async function insertInformations(information_id,
  category_id,
  information_title,
  subject,
  contents,
  viewer,
  is_deleted,
  image_path,
  created_date,
  created_by,
  updated_date,
  updated_by) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "information_id": information_id,
        "category_id": category_id,
        "information_title": information_title,
        "subject": subject,
        "contents": contents,
        "viewer": viewer,
        "is_deleted": is_deleted,
        "image_path": image_path,
        "created_date": created_date,
        "created_by": created_by,
        "updated_date": updated_date,
        "updated_by": updated_by
      }),
    }

    let datas = await fetch(`${urlService}/Informations/InsertInformations`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}

export async function updateInformations(information_id,
  category_id,
  information_title,
  subject,
  contents,
  viewer,
  is_deleted,
  image_path,
  created_date,
  created_by,
  updated_date,
  updated_by) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "information_id": information_id,
        "category_id": category_id,
        "information_title": information_title,
        "subject": subject,
        "contents": contents,
        "viewer": viewer,
        "is_deleted": is_deleted,
        "image_path": image_path,
        "created_date": created_date,
        "created_by": created_by,
        "updated_date": updated_date,
        "updated_by": updated_by
      }),
    }

    let datas = await fetch(`${urlService}/Informations/UpdateInformations`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}

export async function deleteInformations(id) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "id": id
      }),
    }

    let datas = await fetch(`${urlService}/Informations/DeleteInformations`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}