import Constanta from '../helpers/helpers.constanta';
import { stat } from 'react-native-fs';
const urlService = Constanta.urlApi;

export async function getListMemberships(keywords,
  offset,
  limit) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "keywords": keywords,
        "offset": offset,
        "limit": limit
      }),
    }

    let datas = await fetch(`${urlService}/Memberships/GetListMemberships`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}

export async function getDetailMemberships(id) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "id": id
      }),
    }

    let datas = await fetch(`${urlService}/Memberships/GetListMemberships`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}

export async function insertMemberships(membership_id,
  membership_number,
  name,
  education_degree,
  department_id,
  status,
  begining_periode,
  ending_periode,
  image_path,
  is_actived,
  is_deleted,
  created_date,
  created_by,
  updated_date,
  updated_by) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "membership_id": membership_id,
        "membership_number": membership_number,
        "name": name,
        "education_degree": education_degree,
        "department_id": department_id,
        "status": status,
        "begining_periode": begining_periode,
        "ending_periode": ending_periode,
        "image_path": image_path,
        "is_actived": is_actived,
        "is_deleted": is_deleted,
        "created_date": created_date,
        "created_by": created_by,
        "updated_date": updated_date,
        "updated_by": updated_by
      }),
    }

    let datas = await fetch(`${urlService}/Memberships/InsertMemberships`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}

export async function updateMemberships(membership_id,
  membership_number,
  name,
  education_degree,
  department_id,
  status,
  begining_periode,
  ending_periode,
  image_path,
  is_actived,
  is_deleted,
  created_date,
  created_by,
  updated_date,
  updated_by) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "membership_id": membership_id,
        "membership_number": membership_number,
        "name": name,
        "education_degree": education_degree,
        "department_id": department_id,
        "status": status,
        "begining_periode": begining_periode,
        "ending_periode": ending_periode,
        "image_path": image_path,
        "is_actived": is_actived,
        "is_deleted": is_deleted,
        "created_date": created_date,
        "created_by": created_by,
        "updated_date": updated_date,
        "updated_by": updated_by
      }),
    }

    let datas = await fetch(`${urlService}/Memberships/UpdateMemberships`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}

export async function deleteMemberships(id) {
  try {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "id": id
      }),
    }

    let datas = await fetch(`${urlService}/Memberships/DeleteMemberships`, options);
    let result = await datas.json();
    datas = null;
    return result.datas;
  } catch (error) {
    throw error;
  }
}